'use strict';

/**
 * @ngdoc overview
 * @name tinderLanApp
 * @description
 * # tinderLanApp
 *
 * Main module of the application.
 */
angular
  .module('tinderLanApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'gajus.swing',
    'angular-tour',
    'ivpusic.cookie'

  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
