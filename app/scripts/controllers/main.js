'use strict';

/**
 * @ngdoc function
 * @name tinderLanApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the tinderLanApp
 */
angular.module('tinderLanApp')
  .controller('MainCtrl', function ($scope, $http, $document) {
        $scope.cards = [];
        $scope.counter = 0;
        $scope.throwoutCard = 0;

        $scope.throwout = function (eventName, eventObject) {
            $document.find(eventObject.target).hide();
            $scope.throwoutCard++;
            if ($scope.throwoutCard == $scope.cards.length) {
              $scope.getNextComments();
            }
        };

        $scope.throwoutleft = function (eventName, eventObject) {
            //voto negativo
            var commentid = $document.find(eventObject.target).data('commentid');
            var vote = "voteNegative";
            $scope.postCommentVote(vote, commentid);
        };

        $scope.throwoutright = function (eventName, eventObject) {
            //voto positivo
            var commentid = $document.find(eventObject.target).data('commentid');
            var vote = "votePositive";
            $scope.postCommentVote(vote, commentid);
        };

        $scope.throwin = function (eventName, eventObject) {
            //
        };

        $scope.getNextComments = function() {
          $scope.counter += 1;
          $http({
            method : "GET",
            url : "https://latam-customer-reviews.appspot.com/_ah/api/laVoz/v1.2/commentcollection/"+$scope.counter+"/1"
        }).then(function mySucces(response) {
            $scope.cards.unshift.apply($scope.cards,response.data.items);
        }, function myError(response) {
            alert("Ha ocurrido un error");
        });
        }

        $scope.postCommentVote = function (type, commentId) {
          $http({
            method : "POST",
            url : "https://latam-customer-reviews.appspot.com/_ah/api/laVoz/v1.2/"+type+"/"+commentId
        }).then(function mySucces(response) {
        }, function myError(response) {
            alert("Ha ocurrido un error");
        });
        }

});
