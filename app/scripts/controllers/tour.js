angular.module('TourApp', ['angular-tour', 'ivpusic.cookie'])
  .controller('TourCtrl', function($scope, ipCookie) {
  // load from cookie if possible, otherwise set step to 0
  $scope.currentStep = ipCookie('myTour') || 0;
  // callback for when we've finished going through the tour
  $scope.postTourCallback = function() {
    console.log('tour over');
  };
  // optional way of saving tour progress with cookies
  $scope.postStepCallback = function() {
    ipCookie('myTour', $scope.currentStep, { expires: 3000 });
  };
});
